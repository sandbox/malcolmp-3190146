<?php
// $Id$
/**
 * @file
 * Administration page callbacks for csdata module
 */

/**
 * Admin page for Chess Scotland grading file settings
 */
function csdata_admin_files($form, &$form_state) {

  // Default settings
  $settings = csdata_load_settings();

  // Frequency
  $days = $settings['days'];
  $form['days'] = array(
    '#type' => 'textfield',
    '#title' => t('Day of the week'),
    '#description' => t('0=no auto load, 1=Sunday, 2=Monday etc'),
    '#size' => 10,
    '#default_value'  => $days,
    '#weight' => 4,
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('Web file location'),
    '#size' => 60,
    '#default_value'  => $settings['url'],
    '#weight' => 4,
  );

  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#size' => 20,
    '#default_value'  => $settings['user'],
    '#weight' => 4,
  );

  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#size' => 20,
    '#default_value'  => $settings['password'],
    '#weight' => 4,
  );

  $form['getfile'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Get file from web before loading csv'),
   '#default_value'  => 1,
   '#weight' => 9,
  );

  // Submit Button
  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#weight' => 10,
  );

  // New Csv Load.  
  $form['runcsv'] = array(
    '#value' => t('New Load Csv'),
    '#type' => 'submit',
    '#submit'  => array('csdata_run_now'),
    '#weight' => 15,
  );

  return $form;
}

/**
 *  Save Chess Scotland grading files settings.
 */
function csdata_admin_files_submit($form, &$form_state) {
  $settings = array(
    'url' => $form_state['values']['url'],
    'user' => $form_state['values']['user'],
    'password' => $form_state['values']['password'],
    'days' => $form_state['values']['days'],
  );
  csdata_set_load_settings($settings);
  drupal_set_message("Settings Saved");
}

/**
 *  Load Chess Scotland files now.
 */
function csdata_run_batch($queue) {
  $module_file = drupal_get_path('module', 'csdata') .'/csdata.admin.inc';
  $operations = array(
    array('csdata_load_batch', array($queue)),
  );
  $batch = array(
        'operations' => $operations,
        'finished' => 'csdata_load_finished',
        'title'    => 'Loading grade data for chess scotland',
        'init_message' => 'Load Starting ...',
        'file' => $module_file,
  );
  batch_set($batch);
}

function csdata_load_batch($queue, &$context) {
  if(!isset($context['sandbox']['current'])) {
    $nitems = $queue->numberOfItems();
    watchdog('csdata',"Loading data, queue items $nitems");
    $context['sandbox']['queue'] = $queue;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['nrecords'] = $nitems;
  }
  $current = $context['sandbox']['current'];
  $nrecords = $context['sandbox']['nrecords'];
  $queue = $context['sandbox']['queue'];
  if($item = $queue->claimItem()) {
    // Load a batch of records.
    csdata_csv_callback($item->data);
    $queue->deleteItem($item);
  } else {
    $current = $nrecords;
  }
  $current++;
  $context['sandbox']['current'] = $current;
// set finished flag in sandbox
  if( $current >= $nrecords ) {
    $context['finished'] = 1;
    $context['message'] = "Finished reading $nrecords";
  } else {
    $context['finished'] = $current / $nrecords;
    $context['message'] = "Read $current items from $nrecords";
  }
}

function csdata_load_finished($success,$results,$operations) {
  watchdog('csdata', "csdata_load_finished");
  if( $success ) {
    watchdog('csdata', "read succeeded");
    drupal_set_message("read succeeded");
    $last = time();
    $last = variable_set('csdata_last',$last);
    csdata_set_grade_update_date();
    // Let other modules respond to a successful grade load.
    module_invoke_all('csdata_grade_load');
  } else {
    watchdog('csdata', "failed",NULL,WATCHDOG_CRITICAL);
    drupal_set_message("read failed");
    reset($operations);
  }
}

function csdata_run_now($form, &$form_state) {
  $get = $form_state['values']['getfile'];
  $settings = array(
    'url' => $form_state['values']['url'],
    'user' => $form_state['values']['user'],
    'password' => $form_state['values']['password'],
    'days' => $form_state['values']['days'],
  );
  csdata_set_load_settings($settings);
  $queue = csdata_queue_new_file($message,$get,$settings);
  if($queue) {
    csdata_run_batch($queue);
  } else {
    drupal_set_message("Queue creation failed $message");
  }
}
