#!/bin/bash

DATADIR=/var/www/html/sc7/sites/localhost.congress/files/csdata
echo "Getting Clubs ..."
mdb-sql -HFp -d '|' -i clubs.sql -o $DATADIR/clubs.ppe $DATADIR/Grading.mdb

echo "Getting Players ..."
mdb-sql -HFp -d '|' -i players.sql -o $DATADIR/players.ppe $DATADIR/Grading.mdb

echo "Getting Seasons ..."
mdb-sql -HFp -d '|' -i open_seasons.sql -o $DATADIR/seasons.ppe $DATADIR/Grading.mdb

echo "Getting Player Seasons ..."
mdb-sql -HFp -d '|' -i player_seasons.sql -o $DATADIR/player_seasons.ppe $DATADIR/Grading.mdb
