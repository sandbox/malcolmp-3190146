Interface to Chess Scotland grading file
========================================

1) Go to http://www.chessscotland.com/grading/gradeinstall.php
   Select Grading / Area grader.
   and pick on master.zip
    User = areagrader
    PW = Alpha2013

2) Save Master.zip file locally to files/csdata and unzip
3) From modules/custom/csdata run getdata.bash to create pipe(|) delimeted files
    Players (players.ppe)
    Clubs   (clubs.ppe)
    Seasons
    Player Seasons    - rapidplay allegro grade comes from here.
4) These files can then be loaded by going into the admin option and doing
   Run Now (also possibly via cron, but not tested)
5) Upload the .ppe files to vidahost and load there.

Requirements
============

mdb tools.

TODO
====

mdb tools is rubbish and Vidahost doesn't have dbo
but I can run it locally once per year, so its a long term want
(which they say they will do after moving me to a new server). That would be
needed to automated it properly with cron.

Running more often would be useful to get latest PNUM's

Chess Scotland
==============

The latest database (Master.zip which contains grading.mdb) goes up every Wednesday. There is today's http://www.chessscotland.com/grading/gradeinstall.php

    User = areagrader
    PW = Alpha2013

That's cap A on alpha.

To get into the access file the password is BissetBryson
