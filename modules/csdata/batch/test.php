<?php
$mdb_file = "/var/www/drupal-7.41/sites/localhost.congress/files/csdata/Grading.mdb";
$query = 'SELECT * FROM "Players Seasons"';
$driver = 'MDBTools';
if (!file_exists($mdb_file)) {
    die("Could not find database file.");
}
$dataSourceName = "odbc:Driver=$driver;DBQ=$mdb_file;";
$connection = new \PDO($dataSourceName);
$result = $connection->query($query)->fetchAll(\PDO::FETCH_ASSOC);
print_r($result);
?>
