<?php
           
/**
 * @file
 * Drush ECF Data Module.
 *
 * NOTE: when testing run this drush command as the web server user:
 *
 * sudo -u www-data /home/malcolm/bin/vendor/bin/drush erl
 */

/**
 * Implementation of hook_drush_command().
 */
function ecfdata_drush_command() {
  $items['ecf-rating-load'] = array(
    'description' => 'Drush command to load ecf ratings.',
    'aliases' => array('erl'),  // alias of command
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'drupal dependencies' => array('ecfdata', 'file'),
    'examples' => array(        // Listed when user types : drush help lecf
      'ecf-rating-load' => 'Load default ecf ratings',
      'ecf-rating-load list' => 'List the ecf ratings',
      'ecf-rating-load 12' => 'load rating 12 from list',
    ),
  );
  return $items;
}

/*
 * Callback function for hook_drush_command().
 */
function drush_ecfdata_ecf_rating_load() {
  $connection = ecfdata_get_connection();
  // Get arguments passed in command
  $args = func_get_args();
  if($args) {
    foreach($args as $arg){
      drush_print("Got arg $arg");
      if( $arg == 'list') {
        drush_print('Available ratings:');
        $lists = rating_list_list_details('ecf');
        foreach($lists as $lid => $list) {
          $text = $list['name'] . '(' . $list['rate'] . ')';
          drush_print("$lid $text ");
        }

      } else {
        drush_print("Loading rating $arg");
        $dataToLoad = array($arg => $arg);
        if( ecfdata_queue_grades($connection, $message, $dataToLoad) ) {
          drush_print("Loaded rating $arg");
        } else {
          drush_print("Failed to load $arg $message");
        }
      }
    }
  } else {
    drush_print('Getting default ratings ...');
    ecfdata_load_ratings($connection);
  }
  $queue = DrupalQueue::get('ecfdata_load');
  $nitems = $queue->numberOfItems();
  if( $nitems == 0 ) {
    drush_print('No items to load.');
  } else {
    $print = intval($nitems / 10);
    drush_print("Loading ratings $nitems in groups of $print ...");
    $count = 0;
    while($item = $queue->claimItem()) {
      // Load a batch of records.
      ecfdata_load_direct($connection, $item->data);
      $queue->deleteItem($item);
      if($count % $print == 0) {
        drush_print("Loaded $count from $nitems");
      }
      $count++;
    }
  }
  drush_print('Ratings load finished.');
}
