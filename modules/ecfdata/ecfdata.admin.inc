<?php
// $Id$
/**
 * @file
 * Administration page callbacks for ecfdata module
 */

//$exlib = libraries_get_path('vendor');
//require_once "$exlib/autoload.php";
//use \PhpOffice\PhpSpreadsheet\Spreadsheet;
//use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Admin page for uploading new ECF grading master or club files
 */
function ecfdata_admin_files($form, &$form_state) {

  // Required to do file uploads
  $form['#attributes'] = array('enctype' => "multipart/form-data");
 
  // Saved variables
  $connection = ecfdata_get_connection();

  // OTB field set

  $form['direct'] = array(
   '#title' => t('Monthly Grading System'),
   '#type'  => 'fieldset',
   '#weight' => 30,
  );

  $form['direct']['mgsurl'] = array(
   '#type'  => 'textfield',
   '#size'  => 50,
   '#maxlength'  => 70,
   '#title'  => t('Grading System URL'),
   '#required' => true,
   '#default_value'  => $connection->mgsurl,
   '#weight' => 10,
  );

  $form['direct']['username'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#maxlength'  => 30,
   '#title'  => t('User Name'),
   '#required' => true,
   '#default_value'  => $connection->username,
   '#weight' => 20,
  );

  $form['direct']['password'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#maxlength'  => 30,
   '#title'  => t('Password'),
   '#required' => true,
   '#default_value'  => $connection->password,
   '#weight' => 30,
  );

  $form['direct']['cron'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Load ratings when cron runs'),
   '#default_value'  => $connection->cron,
   '#weight' => 50,
  );

  // Save Button
  $form['direct']['save'] = array(
    '#value' => t('Save Settings'),
    '#type' => 'submit',
    '#submit' => array('ecfdata_admin_run_save'),
  );

  // Rating lists field set.
  $form['lists'] = array(
   '#title' => t('Data to Load'),
   '#type'  => 'fieldset',
   '#weight' => 40,
  );

  // Submit Button
  $form['lists']['run'] = array(
    '#value' => t('Run Monthly Grading Load Now'),
    '#type' => 'submit',
    '#submit' => array('ecfdata_admin_run_submit'),
  );

  $opts = array(
    'clubs' => t('Clubs'),
    'players' => t('Players'),
  );
  $defaults = array('clubs' => 0, 'players' => 0, );
  $lists = rating_list_list_details('ecf');
  foreach($lists as $lid => $list) {
    $opts[strval($lid)] = $list['name'] . '(' . $list['rate'] . ')';
    $defaults[strval($lid)] = 0;
  }

  $form['lists']['toload'] = array(
   '#title'  => t('Settings'),
   '#type'  => 'checkboxes',
   '#default_value'  => $defaults,
   '#description' => t('Data to load.'),
   '#options'  => $opts,
  );

  // Debug field set

  $form['debug'] = array(
   '#title' => t('Debug'),
   '#type'  => 'fieldset',
   '#weight' => 40,
  );

  $opts = array(
    'debug' => t('Output debug info and load only 5 records'),
  );
  $genopts = array('debug' => 0, );

  $form['debug']['genopts'] = array(
   '#title'  => t('Settings'),
   '#type'  => 'checkboxes',
   '#default_value'  => $genopts,
   '#description' => t('For debugging purposes only. Leave unchecked.'),
   '#options'  => $opts,
  );

  return $form;
}

function ecfdata_admin_get_db($form, &$form_state) { 
  $username = $form_state['values']['username'];
  $password = $form_state['values']['password'];
  $mgsurl = $form_state['values']['mgsurl'];
  $cron = $form_state['values']['cron'];
  $default = ecfdata_default_connection();
  $default->username = $username;
  $default->password = $password;
  $default->mgsurl = $mgsurl;
  $default->cron = $cron;
  return $default;
}

/**
 *  Action after the button to save monthly grading settings is pressed.
 */

function ecfdata_admin_run_save($form, &$form_state) {
  $connection = ecfdata_admin_get_db($form, $form_state);
  variable_set('ecfdata_direct',$connection);
  drupal_set_message('Values Saved');
}

/**
 *  Action after the button to run the monthly grading load is pressed.
 */
function ecfdata_admin_run_submit($form, &$form_state) {
  $connection = ecfdata_admin_get_db($form, $form_state);
  variable_set('ecfdata_direct',$connection);
  $debug = FALSE;
  $debugOption = $form_state['values']['genopts']['debug'];
  if($debugOption) {
    $debug = TRUE;
  }
  $dataToLoad = $form_state['values']['toload'];
  drupal_set_message('Loading ...');

  // Queue updates
  if(!ecfdata_queue_updates($connection, $dataToLoad, FALSE)) {
    drupal_set_message("Queue updates failed", 'error');
    return;
  }
  // Setup batch to process the queues
  $queue = DrupalQueue::get('ecfdata_load');
  ecfdata_run_batch($connection, $queue);

  // set date of last update
  ecfdata_set_grade_update_date();
}

function ecfdata_run_batch($connection, $queue) {
  $module_file = drupal_get_path('module', 'ecfdata') .'/ecfdata.admin.inc';
  $operations = array(
    array('ecfdata_mgs_load_batch', array($connection, $queue)),
  );
  $batch = array(
        'operations' => $operations,
        'finished' => 'ecfdata_mgs_load_finished',
        'title'    => 'Loading data from monthly grading',
        'init_message' => 'Load Starting ...',
        'file' => $module_file,
  );
  batch_set($batch);
}

function ecfdata_mgs_load_batch($connection, $queue, &$context) {
  if(!isset($context['sandbox']['current'])) {
    $nitems = $queue->numberOfItems();
    watchdog('ecfdata',"Loading data, queue items $nitems");
    $context['sandbox']['queue'] = $queue;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['nrecords'] = $nitems;
  }
  $current = $context['sandbox']['current'];
  $nrecords = $context['sandbox']['nrecords'];
  $queue = $context['sandbox']['queue'];
  if($item = $queue->claimItem()) {
    // Load a batch of records.
    ecfdata_load_direct($connection, $item->data);
    $queue->deleteItem($item);
  } else {
    $current = $nrecords;
  }
  $current++;
  $context['sandbox']['current'] = $current;

  // set finished flag in sandbox
  if( $current >= $nrecords ) {
    $context['finished'] = 1;
    $context['message'] = "Finished reading $nrecords";
  } else {
    $context['finished'] = $current / $nrecords;
    $context['message'] = "Read $current items from $nrecords";
  }
}

function ecfdata_mgs_load_finished($success,$results,$operations) {
  watchdog('ecfdata', "ecfdata_mgs_load_finished");
  if( $success ) {
    watchdog('ecfdata', "read from mgs succeeded");
    // Let other modules respond to a successful membership load.
    // Let other modules respond to a successful grade load.
    module_invoke_all('ecfdata_grade_load');
  } else {
    watchdog('ecfdata', "grading data load failed",NULL,WATCHDOG_CRITICAL);
    reset($operations);
  }
}
