#!/bin/bash

echo "Calling PHP to load FIDE file for LMS"
php -d memory_limit=128M -f /home/lms/public_html/lms/sites/all/modules/custom/rating_list/modules/fidedata/batch/split_fide_file.php http://ratings.fide.com/download/players_list_xml.zip /home/lms/public_html/lms/sites/default/files/fidedata A
echo "PHP ended"
