<?php
// $Id$
/**
 * @file
 * Administration page callbacks for fidedata module
 */

/**
 * Admin page for FIDE grading file settings
 */
function fidedata_admin_files($form, &$form_state) {

  // Frequency
  $days = 0;
  $days = variable_get('fidedata_days',$days);
  $form['days'] = array(
    '#type' => 'textfield',
    '#title' => t('Day of month to run'),
    '#description' => t('Zero to not run'),
    '#size' => 10,
    '#default_value'  => $days,
    '#weight' => 4,
  );

  $plp = FALSE;
  $plp = variable_get('fidedata_plp',$plp);
  $form['plp'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Load player_list_player table'),
   '#default_value'  => $plp,
   '#description' => t('Not needed for LMS because just link via FIN'),
   '#weight' => 8,
  );

  $web = TRUE;
  $form['web'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Download file from web'),
   '#default_value'  => $web,
   '#description' => t('Unclick for testing the same file'),
   '#weight' => 8,
  );

  // Submit Button
  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#weight' => 10,
  );

  // Run Now Button
  $form['run'] = array(
    '#value' => t('Run Now'),
    '#type' => 'submit',
    '#submit'  => array('fidedata_run_now'),
    '#weight' => 15,
  );

  return $form;
}

/**
 *  Save FIDE grading files settings.
 */
function fidedata_admin_files_submit($form, &$form_state) {
  $days = $form_state['values']['days'];
  variable_set('fidedata_days', $days);
  $plp = $form_state['values']['plp'];
  variable_set('fidedata_plp',$plp);
}

/**
 *  Load FIDE files now.
 */

function fidedata_run_now($form, &$form_state) {
  $days = $form_state['values']['days'];
  variable_set('fidedata_days', $days);
  $plp = $form_state['values']['plp'];
  variable_set('fidedata_plp',$plp);
  $web= $form_state['values']['web'];

  $queue = fidedata_queue_files($web);

  fidedata_run_batch($queue);
  $now = time();
  variable_set('fidedata_last', $now);
}

function fidedata_run_batch($queue) {
  $module_file = drupal_get_path('module', 'fidedata') .'/fidedata.admin.inc';
  $operations = array(
    array('fidedata_load_file_batch', array($queue)),
  );
  $batch = array(
        'operations' => $operations,
        'finished' => 'fidedata_read_finished',
        'title'    => 'Loading FIDE Rating Files',
        'init_message' => 'Load Starting ...',
        'file' => $module_file,
  );
  batch_set($batch);
}

function fidedata_load_file_batch($queue, &$context) {
  if(!isset($context['sandbox']['current'])) {
    $nitems = $queue->numberOfItems();
    watchdog('fidedata',"Loading data, queue items $nitems");
    $context['sandbox']['queue'] = $queue;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['nrecords'] = $nitems;
  }
  $current = $context['sandbox']['current'];
  $nrecords = $context['sandbox']['nrecords'];
  $queue = $context['sandbox']['queue'];
  if($item = $queue->claimItem()) {
    fidedata_load_callback($item->data);
    $queue->deleteItem($item);
  } else {
    $current = $nrecords;
  }
  $current++;
  $context['sandbox']['current'] = $current;

  // set finished flag in sandbox
  if( $current >= $nrecords ) {
    $context['finished'] = 1;
    $context['message'] = "Finished reading $nrecords";
    drupal_set_message("Read $nrecords files");
  } else {
    $context['finished'] = $current / $nrecords;
    $context['message'] = "Read $current files from $nrecords";
  }
}

function fidedata_read_finished($success,$results,$operations) {
  watchdog('fidedata', "fidedata_read_finished");
  if( $success ) {
    watchdog('fidedata', "read ratings succeeded");
    drupal_set_message("Ratings Loaded Successfully");
  } else {
    watchdog('fidedata', "ratings load failed",NULL,WATCHDOG_CRITICAL);
    reset($operations);
  }
}
