<?php
/**
 * @file
 * Install, update and uninstall functions for the fidedata module.
 *
 */

// $Id$

/**
 * Implementation of hook_install().
 */

function fidedata_install() {
  fidedata_create_rating_lists();
}

/**
 * Implementation of hook_uninstall().
 */
function fidedata_uninstall() {
  fidedata_delete_rating_lists();
}

/**
 *  Update dependencies.
 */
function fidedata_update_dependencies() {
  // fidedata_update_7002() function must run after rating_list_update_7003
  $dependencies['fidedata'][7002] = array(
    'rating_list' => 7003,
  );
  return $dependencies;
}


/**
 * Implementation of hook_schema().
 */
function fidedata_schema() {

  // FIDE Master table
  
  $schema['fidedata_players'] = array(
  'description' => 'FIDE Grading List',
  'fields' => array(
    'fideid' => array(
      'description' => 'FIDE ID',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ),
    'name' => array(
      'description' => 'Player Full Name',
      'type' => 'varchar',
      'length' => '60',
      'not null' => TRUE,
    ),
    'sex' => array(
      'description' => 'Player SEX',
      'type' => 'char',
      'length' => '1',
      'not null' => FALSE,
    ),
    'country' => array(
      'description' => 'Player Country',
      'type' => 'char',
      'length' => '3',
      'not null' => FALSE,
    ),
    'rating' => array(
      'description' => 'Rating',
      'type' => 'int',
      'not null' => TRUE,
      'default'  => 0,
    ),
    'rapid_rating' => array(
      'description' => 'Rating',
      'type' => 'int',
      'not null' => TRUE,
      'default'  => 0,
    ),
    'blitz_rating' => array(
      'description' => 'Rating',
      'type' => 'int',
      'not null' => TRUE,
      'default'  => 0,
    ),
  ),
  'primary key' => array('fideid'),
  'indexes' => array(
    'NAME' => array('name'),
  ),
  );

  return $schema;
}

/**
 * Create player list and rating lists on install
 */
function fidedata_create_rating_lists() {
  // Create rating lists
  $standard = entity_get_controller('rating_list')->create(-2);
  $standard->name = 'FIDE Standard';
  $standard->rate = 'S';
  $standard->module = 'fide';
  $standard->list = 1;
  rating_list_save($standard);
  $rapid = entity_get_controller('rating_list')->create(-2);
  $rapid->name = 'FIDE Rapid';
  $rapid->rate = 'R';
  $rapid->module = 'fide';
  $rapid->list = 1;
  rating_list_save($rapid);
  $blitz = entity_get_controller('rating_list')->create(-2);
  $blitz->name = 'FIDE Blitz';
  $blitz->rate = 'B';
  $blitz->module = 'fide';
  $blitz->list = 1;
  rating_list_save($blitz);
}

/**
 * Delete rating lists on uninstall
 */
function fidedata_delete_rating_lists() {
  $rapid = rating_list_getid('R',1,-2);
  rating_list_delete($rapid);
  $standard = rating_list_getid('S',1,-2);
  rating_list_delete($standard);
  $blitz = rating_list_getid('B',1,-2);
  rating_list_delete($blitz);
}

