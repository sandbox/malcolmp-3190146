Command line prog to parse FIDE data file
=========================================

Command To test small file locally:
php -f split_fide_file.php file:///var/www/drupal-7.50/sites/all/modules/custom/fidedata/players_list_xml.zip /var/www/drupal-7.50/sites/all/modules/custom/fidedata/batch/testdir

Command To test whole file locally:
php -f split_fide_file.php http://ratings.fide.com/download/players_list_xml.zip /var/www/drupal-7.50/sites/all/modules/custom/fidedata/batch/testdir

Command To run whole file locally
php -f split_fide_file.php http://ratings.fide.com/download/players_list_xml.zip /var/www/drupal-7.50/sites/localhost.congress/files/fidedata

Command to test small file on vidahost:
php -f /home/popmalco/www/drupal/sites/all/modules/custom/fidedata/batch/split_fide_file.php file:///home/popmalco/www/drupal/sites/all/modules/custom/fidedata/batch/players_list_xml.zip /home/popmalco/www/drupal/sites/congress.popmalc.org.uk/files/fidedata

Command To test whole file on vidahost:
php -f /home/popmalco/www/drupal/sites/all/modules/custom/fidedata/batch/split_fide_file.php http://ratings.fide.com/download/players_list_xml.zip /home/popmalco/www/drupal/sites/congress.popmalc.org.uk/files/fidedata

note there is a script file fideload.bash with this in on vidahost in
~/www/drupal/sites/all/modules/custom/fidedata
run this, then go into FIDE Data Settings and click "Run Now"
