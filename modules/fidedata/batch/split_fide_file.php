<?php
/*
 * Stand alone php command line program to parse the fide xml file and
 * split it into multiple pipe delimited files so that they can be 
 * loaded seperately by drupal cron
 *
 */

class FideXmlParser {
  var $player;
  var $name;
  var $players;
  var $filter;
  var $count;
  var $fileno;
  var $batch;
  var $message;
  var $fout;
  var $out_dir;

  public function getMessage() {
    return $this->message;
  }

  public function FideXmlParser() {
    $this->count = 0;      // Count in this file
    $this->total = 0;      // Total records
    $this->fileno = 1;     // Count of number of output files
    $this->filter = 'U';   // Filter out UK only
    $this->batch = 1000;   // Number of records per file. 
    $this->debug = FALSE;  // Debug flag
  }
 
  public function start_element($parser, $name, $attrs) {
    $this->name = $name;
    if($this->debug) {
      echo "start_element $this->name\n";
    }
    if($name == 'player') {
      if($this->player->fideid != 0) {
        if($this->want_player() ) {
          if($this->count > $this->batch) {
            fclose($this->fout);
            $this->fileno++;
            $this->count=0;
            $out_file = 'file' . $this->fileno;
            echo "Total = $this->total Writing to $out_file ... \n";
            if (!($this->fout = fopen($out_file, "w"))) {
              $this->message = "could not open output file";
              die;
            }
          }
          $this->count++;
          $this->total++;
          $this->print_player();
        }
      }
      $this->player = $this->create_player();
    }

  }
  public function end_element($parser, $name) {
//  echo "end_element $this->name\n";
  }
  public function character_data($parser, $data) {
//  echo "character_data $this->name $data\n";
    if(!isset($data) || strlen($data)==0 || $data == ' ' ) {
      return;
    }
    switch($this->name) {
       case 'name' :
       case 'sex' :
       case 'country' : $this->store_value($data, FALSE);
                             break;
       case 'fideid' :  if($this->debug) {
                          echo "fideid $data\n";
                        }
       case 'rating' :
       case 'rapid_rating' :
       case 'blitz_rating' : $this->store_value($data, TRUE);
    }

  }

  public function create_player() {
  return (object) array(
      'fideid' => 0,
      'name' => '',
      'sex' => '',
      'country' => '',
      'rating' => 0,
      'rapid_rating' => 0,
      'blitz_rating' => 0,
    );
  }

  public function store_value($value, $int=FALSE) {
    $name = $this->name;
    if(isset($value)) {
      if($int && strlen($value)==0 ) {
        $this->player->$name = 0;
      } else {
        $this->player->$name = $value;
      }
    }
  }


  public function print_player() {
    fprintf($this->fout,'%s|',$this->player->fideid);
    fprintf($this->fout,'%s|',$this->player->name);
    fprintf($this->fout,'%s|',$this->player->sex);
    fprintf($this->fout,'%s|',$this->player->country);
    fprintf($this->fout,'%s|',$this->player->rating);
    fprintf($this->fout,'%s|',$this->player->rapid_rating);
    fprintf($this->fout,"%s\n",$this->player->blitz_rating);
  }

  public function parse_file($in_file, $out_dir) {
    $this->out_dir = $out_dir;
    $xml_parser = xml_parser_create();
//  xml_set_element_handler($xml_parser, "start_element", "end_element");
    xml_set_element_handler($xml_parser, array($this,"start_element"), array($this,"end_element") );
    xml_set_character_data_handler($xml_parser, array($this,"character_data") );
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, 0);
    if (!($fp = fopen($in_file, "r"))) {
      $this->message = "could not open XML input";
      return FALSE;
    }
    $out_file = 'file' . $this->fileno;
    echo "Total = $this->total Writing to $out_file ... \n";
    if (!($this->fout = fopen($out_file, "w"))) {
      $this->message = "could not open output file";
      return FALSE;
    }
    $this->player = $this->create_player();
    while ($data = fread($fp, 4096)) {
      $data = str_replace(array("\n", "\r", "\t", "&", ";"), '', $data);
      if (!xml_parse($xml_parser, $data, feof($fp))) {
        $this->message = sprintf("XML error: %s at line %d",
                      xml_error_string(xml_get_error_code($xml_parser)),
                      xml_get_current_line_number($xml_parser));
        return FALSE;
      }
    }
    xml_parser_free($xml_parser);
    fclose($this->fout);
    echo "Parse completed: $this->total records \n";

    return TRUE;
  }

  function want_player() {
    if($this->filter == 'U') {
      if($this->player->country != 'ENG' &&
         $this->player->country != 'SCO' &&
         $this->player->country != 'WLS' &&
         $this->player->country != 'IRL' ) {
        return FALSE;
      }
    }
    return TRUE;
  }
}

function get_web_page( $url ){
  $options = array(
    CURLOPT_RETURNTRANSFER => true,     // return web page
    CURLOPT_HEADER         => false,    // don't return headers
    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    CURLOPT_ENCODING       => "",       // handle all encodings
    CURLOPT_USERAGENT      => "spider", // who am i
    CURLOPT_AUTOREFERER    => true,     // set referer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    CURLOPT_TIMEOUT        => 120,      // timeout on response
    CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
  );

  $ch      = curl_init( $url );
  curl_setopt_array( $ch, $options );
  $content = curl_exec( $ch );
  curl_close( $ch );

  return $content;
}

echo "Program to parse FIDE xml file\n";
$start_time = time();
// Set filter - U for UK only
$filter = 'U';
if ( isset($argv[3]) ) {
  $filter = $argv[3];
}
if (isset($argv[1]) && isset($argv[2]) ) {
  $url = $argv[1];        // url of the fide file
  $out_dir = $argv[2];    // fully qualified directory name**
  echo 'Will read from: '. $url . ' write to directory: ' . $out_dir . " \n";
  echo 'Filter: ' . $filter . " \n";

  if(! chdir($out_dir) ) {
    echo "Failed to change directory to $out_dir ... \n";
    exit();
  }

  $filename = 'fidedata.zip';

  echo "Getting file $url ... \n";
  $contents = get_web_page($url);
  echo "Saving file $url ... \n";
  $handle = fopen($filename,"w");
  fwrite($handle,$contents);
  echo "$filename saved \n";
  fclose($handle);

  echo "Opening zip archive $filename ... \n";
  $za = new ZipArchive();
  $status = $za->open($filename );
  if( $status === false ) {
    echo "Failed to open zip file $filename \n";
    exit();
  }
  $dir = $out_dir;
  echo "Unzipping file $filename to $dir ... \n";
  $status = $za->extractTo( $dir );
  if( $status === false ) {
    echo "Failed to extract zip file $realname to $dir \n";
    exit();
  }

  $in_file = $dir . '/players_list_xml_foa.xml';

  // Remove all files in the output directory
  echo "Clearing directory $out_dir ... \n";
  array_map('unlink', glob("$out_dir/file*"));

  echo "Parsing xml file $in_file ... \n";
  $parser = new FideXmlParser();
  $parser->filter = $filter;
  if(!$parser->parse_file($in_file, $out_dir)) {
    echo 'FAILED: ' . $parser->getMessage();
  } else {
    echo "Success \n";
  }
  
} else {
  echo "Incorrect number of arguments \n";
}
$end_time = time();
$seconds = $end_time - $start_time;
echo "Completed in $seconds seconds \n";
?>
