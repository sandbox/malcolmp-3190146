<?php
/**
* @file
* Functions for rating list view.
*/

function rating_list_view_page($list, $view_mode = 'full') {

  $rlid = $list->rlid;
  $list->content = array();

  // Build fields content
  field_attach_prepare_view('rating_list',
                            array($list->rlid => $list),
                             $view_mode);

  $rows = array();
  $rows[] = array('List ID', $list->rlid);
  $rows[] = array('Name', $list->name);
  $rows[] = array('Rate', $list->rate);
  $rows[] = array('Type', $list->module);
 
  $list->content['table'] = array(
    '#theme' => 'table',
    '#rows'  => $rows,
  );

  $list->content += field_attach_view('rating_list', $list, $view_mode);
 
  return $list->content;
}
