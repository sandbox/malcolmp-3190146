<?php
/**
 * @file
 * Rating controller
 */

class RatingListController extends DrupalDefaultEntityController {
  public function create($org) {
    return (object) array(
      'rlid' => '',
      'org' => $org,
      'rate' => 'S',
      'name' => '',
      'module' => '',
      'list' => 0,
    );
  }

  public function save($rating_list) {
    $transaction = db_transaction();

    try {
      $rating_list->is_new = empty($rating_list->rlid);
      if($rating_list->is_new) {
        drupal_write_record('rating_list', $rating_list);
        $op = 'insert';
      } else {
        drupal_write_record('rating_list', $rating_list, 'rlid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('rating_list', $rating_list);

      module_invoke_all('entity_' . $op, $rating_list, 'rating_list');
      unset($rating_list->is_new);

      db_ignore_slave();

      return $rating_list;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('rating_list', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($rlids) {
    if(!empty($rlids)) {
      $rating_lists = $this->load($rlids, array());
      $transaction = db_transaction();

      try {
        db_delete('rating_list')
          ->condition('rlid', $rlids, 'IN')
          ->execute();

        foreach($rating_lists as $rlid => $rating_list) {
          field_attach_delete('rating_list', $rating_list);
          module_invoke_all('entity_delete', $rating_list, 'rating_list');
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('rating_list', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}
