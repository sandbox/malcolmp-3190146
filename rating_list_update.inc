<?php
/**
* @file
* Functions for rating list updates.
*/

/**
* Add new rating list.
*/
function rating_list_add() {
  $list = entity_get_controller('rating_list')->create(0);
  return drupal_get_form('rating_list_update_form', $list);
}

/**
*  Form for updating rating list details.
*/
function rating_list_update_form($form, &$form_state,$list) {

  $form['#rating_list'] = $list;
  $form_state['rating_list'] = $list;

  $form['add'] = array(
   '#title' => t('Rating list details'),
   '#type'  => 'fieldset',
   '#attributes' => array(
                     'class' => array('update-rating-fieldset'),
                    )
  );

  if(empty($list->rlid)) {
    $btitle = 'Add';
  } else {
    $btitle = 'Update';
  }

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
     '#value' => t($btitle),
     '#type'  => 'submit',
     '#weight' => 5,
  );
  if(!empty($list->rlid)) {
    $form['buttons']['delete'] = array(
       '#value' => t('Delete'),
       '#type'  => 'submit',
       '#submit'  => array('rating_list_update_delete'),
       '#weight' => 15,
    );
  }
  $form['buttons']['cancel'] = array(
    '#value' => t('Cancel'),
    '#type'  => 'submit',
    '#submit'  => array('rating_list_update_cancel'),
    '#weight' => 25,
  );

  $form['add']['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Name'),
   '#default_value'  => $list->name,
   '#weight' => 15,
   '#required' => TRUE,
  );

  $rates = rating_list_rates();
  $form['add']['rate'] = array(
   '#type' => 'select',
   '#title'  => t('Rate'),
   '#options' => $rates,
   '#default_value'  => $list->rate,
   '#weight' => 25,
  );

  $types = rating_list_types();
  $form['add']['type'] = array(
   '#type' => 'select',
   '#title'  => t('Type'),
   '#options' => $types,
   '#default_value'  => $list->module,
   '#weight' => 25,
  );

  // Add in entity fields
  field_attach_form('rating_list', $list, $form, $form_state);

  return $form;
}

/**
*  Action after the cancel button is pressed.
*/
function rating_list_update_cancel($form, &$form_state) {
  $url = 'admin/config/media/rating';
  drupal_goto($url);
}

/**
*  Action after the delete button is pressed.
*/
function rating_list_update_delete($form, &$form_state) {
  $list = $form_state['rating_list'];
  drupal_set_message("Deleting list $list->rlid ...");
}

/**
*  Action after the form is submitted.
*  Update rating list record
*/
function rating_list_update_form_submit($form, &$form_state) {
  $list = $form_state['rating_list'];
  $list->name = $form_state['values']['name'];
  $list->rate = $form_state['values']['rate'];
  $list->module = $form_state['values']['type'];

  // Notify field widgets
  field_attach_submit('rating_list', $list, $form, $form_state);

  // Save List
  rating_list_save($list);

  drupal_set_message(t('List Saved'));
}

function rating_list_edit_page($list) {
  // Show form
  return drupal_get_form('rating_list_update_form', $list);
}
