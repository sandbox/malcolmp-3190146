<?php
/**
* @file
* Functions for rating lists options.
*/

function rating_list_lists_page() {
  drupal_set_message("Ratings lists");
  $content['top']['#markup'] = l('Add Rating List', 'rating_listadd');

  $content['lists']['#table'] = rating_lists_table();
  $content['lists']['#theme'] = 'league_table';

  return $content;
}

function rating_lists_table() {

  $header = array(array('data' => 'List Name', 'field' => 'name' ),
                  array('data' => 'Rate ', 'field' => 'rate' ),
                  array('data' => 'Type', 'field' => 'module' ),
  );

  $query = db_select('rating_list', 'r');
  $query->fields('r', array('rlid', 'rate', 'name', 'module'));
  $query = $query->extend('TableSort')->orderByHeader($header);
  $rows = array();

  $result = $query->execute();
  foreach($result as $row) {
    $name = l($row->name, 'rating_list/' . $row->rlid);
    $rate = $row->rate;
    $type = $row->module;
    $rows[] = array($name, $rate, $type);
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}
